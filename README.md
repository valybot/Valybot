# Valybot

## Description
A bot created for Valynne's FFXIV Samurai Community Discord. Aims to replace Kupobots functionality of !faq as it is not editable for their members.


## Requirements

Python3-packages:
- py-cord installation via 
```
git clone https://github.com/Pycord-Development/pycord  -  pip install -U .
```
- python-dotenv

## Installation
Unpack it somewhere and add an .env with the following content:
```
TOKEN = <your-bot-token>
DB = <path/to/sqlite/dbfile.db>
ADMINGROUP = <role ID that is allowed to execute administrative commands>
```

## Available commands
- !faq [add/addalias/delete/edit]

## Support
ask Lopu, this isn't supposed to run outside Valycord anyway
