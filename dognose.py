import discord
from discord.ext import commands
import os
from dotenv import load_dotenv
import sqlite3
import sys



intents = discord.Intents.default()
intents.messages = True
intents.message_content = True

load_dotenv()
bot = commands.Bot(command_prefix="!", intents=intents)

if not os.path.isfile(os.getenv('DB')):
    print(f'I was not able to find my database. Please look at the problem - maybe you did not initialize it?')
    sys.exit()

@bot.event
async def on_ready():
    print(f"{bot.user} is online.")

async def check_permission(ctx):
    test = os.getenv('ADMINGROUP')
    role = discord.utils.get(ctx.guild.roles, id=int(os.getenv('ADMINGROUP')))
    #if os.getenv('ADMINGROUP') in ctx.author.roles:
    if role in ctx.author.roles:
        return True
    else:
        await ctx.reply("You do not have the permissions to do this.")
        return False


async def writefaq(ctx, name, title, text, imgurl=None):
    if await check_permission(ctx) == False:
        return None
    faqconnection = sqlite3.connect(os.getenv('DB'))
    faqcursor = faqconnection.cursor()
    searchname = name + '%'
    faqcursor.execute("SELECT count(*) FROM commands WHERE name LIKE ?", (searchname,))
    data=faqcursor.fetchone()

    name = name.lower()
    if data[0]>0: 
        await ctx.reply("That FAQ command already exists.")
    elif data[0] == 0:
        faqcursor.execute("INSERT INTO commands(name) VALUES(?)", (name,))
        last_row_id = faqcursor.lastrowid
        faqcursor.execute("UPDATE commands SET contentid = ? WHERE id = ?", (last_row_id, last_row_id,))
        if imgurl is None:
            sql = '''INSERT INTO content(id, title, text) VALUES(?,?,?)
            '''
            task = (last_row_id, title, text)
        else:
            sql = '''INSERT INTO content(id, title, text, imgurl) VALUES(?,?,?,?)
            '''
            task = (last_row_id, title, text, imgurl)
        
        faqcursor.execute(sql, task)
        faqconnection.commit()
        faqconnection.close()
        await ctx.reply("FAQ Command saved!")

async def readfaq(ctx, name):
    name = name.lower()
    faqconnection = sqlite3.connect(os.getenv('DB'))
    faqcursor = faqconnection.cursor()
    searchname = name + '%'
    faqcursor.execute("SELECT contentid FROM commands WHERE name LIKE ?", (searchname,))
    data=faqcursor.fetchall()
    datalength=len(data)

    if datalength == 0:
        await ctx.reply("I couldn't find that FAQ command.")
        return None
    elif datalength == 1:
        faqcursor.execute("SELECT * FROM content WHERE id = ?", (data[0][0],))
        faqcontent = faqcursor.fetchone()
        embed = discord.Embed(
            title = faqcontent[1],
            description = faqcontent[2],
            color = discord.Colour.orange()
        )
        if faqcontent[3]:
            embed.set_image(url = faqcontent[3])
    elif datalength > 1:
        faqcursor.execute("SELECT name FROM commands WHERE name LIKE ?", (searchname,))
        data=faqcursor.fetchall()
        outputstring = ""
        for i in data:
            outputstring += i[0]
            outputstring += "\n"
        embed = discord.Embed(
        title = "Multiple commands found! Choose one:",
        description = outputstring,
        color = discord.Colour.orange()
        )
    await ctx.reply(embed=embed)

async def addalias(ctx, oldname, newname):
    if await check_permission(ctx) == False:
        return None
    for i in oldname, newname:
        i = i.lower()
    faqconnection = sqlite3.connect(os.getenv('DB'))
    faqcursor = faqconnection.cursor()
    searchname = oldname + '%'
    faqcursor.execute("SELECT count(*) FROM commands WHERE name LIKE ?", (searchname,))
    data=faqcursor.fetchone()

    if data[0] == 0: 
        await ctx.reply("I was not able to find that FAQ command.")

    elif data[0] > 1:
        faqcursor.execute("SELECT name FROM commands WHERE name LIKE ?", (searchname,))
        data=faqcursor.fetchall()
        faqlist = ""
        for i in data:
            faqlist += data[i] + '\n'

        embed = discord.Embed(
            description=faqlist,
            color = discord.Colour.orange()
        )
        await ctx.reply("I was able to find more than one command that matches the old name. See all matches below:", embed=embed)

    elif data[0] == 1:
        faqcursor.execute("SELECT contentid FROM commands WHERE name LIKE ?", (searchname,))
        data=faqcursor.fetchone()
        faqcursor.execute("INSERT INTO commands(name, contentid) VALUES (?,?)", (newname, data[0]))
        faqconnection.commit()
        faqconnection.close()
        response="I added " + newname + " as an alias for " + oldname +". bork"
        await ctx.reply(response)
        

async def editfaq(ctx, name, title, text, imgurl=None):
    if await check_permission(ctx) == False:
        return None
    faqconnection = sqlite3.connect(os.getenv('DB'))
    faqcursor = faqconnection.cursor()
    searchname = name + '%'
    faqcursor.execute("SELECT count(*) FROM commands WHERE name LIKE ?", (searchname,))
    data=faqcursor.fetchone()

    name = name.lower()
    if data[0]!=1: 
        await ctx.reply("I did not find exactly 1 match for your FAQ name - it was " + data[0] + " matches.")
    elif data[0] == 1:
        faqcursor.execute("SELECT contentid FROM commands WHERE name = ?", (name,))
        contentid = faqcursor.fetchone()
        if imgurl is None:
            faqcursor.execute("UPDATE content SET title = ?, text = ?, imgurl = NULL WHERE id = ?", (title, text, contentid[0]))
        else:
             faqcursor.execute("UPDATE content SET title = ?, text = ?, imgurl = ? WHERE id = ?", (title, text, imgurl, contentid[0]))
        faqconnection.commit()
        faqconnection.close()
        await ctx.reply("FAQ Command successfully edited!")

async def deletefaq(ctx, name):
    if await check_permission(ctx) == False:
        return None
    faqconnection = sqlite3.connect(os.getenv('DB'))
    faqcursor = faqconnection.cursor()
    searchname = name + '%'
    faqcursor.execute("SELECT count(*) FROM commands WHERE name LIKE ?", (searchname,))
    data=faqcursor.fetchone()

    name = name.lower()
    if data[0]!=1: 
        await ctx.reply("I did not find exactly 1 match for your FAQ name - it was " + data[0] + " matches.")
    elif data[0] == 1:
        # we are only deleting the entry from commands table. thus the content itself is still available and can be recovered
        faqcursor.execute("DELETE FROM commands WHERE name = ?", (name,))
        faqconnection.commit()
        faqconnection.close()
        await ctx.reply("FAQ Command successfully deleted!")

@bot.command(name = "boop", description = "Boop the snoot")
async def boop(ctx):
    print(f"Received boop command")
    await ctx.reply("Hey! My snoot!")


@bot.command(name = "faq", description = "Read a FAQ-Answer curated by this discord's community.")
async def faq(ctx, *args):
    if args[0] == "add":
        if len(args) != 4:
            await ctx.reply('Wrong amount of args, please make sure you do !faq add "Name" "Title" "Body"')
        else:
            if len(ctx.message.attachments) == 0:
                await writefaq(ctx, args[1], args[2], args[3])
            elif len(ctx.message.attachments) == 1:
                await writefaq(ctx, args[1], args[2], args[3], ctx.message.attachments[0].url)
            else:
                await ctx.reply('I received too many attachments and am confused, contact Lopu pls')
    elif args[0] == "addalias":
        if len(args) != 3:
            await ctx.reply('Please use it like this: `!faq addalias oldname newname`')
        else:
            await addalias(ctx, args[1], args[2])
    elif args[0] == "edit":
        if len(args) != 4:
            await ctx.reply('Please use it like this: `!faq edit faqname "Title" "Body"`')
        else:
            if len(ctx.message.attachments) == 0:
                await editfaq(ctx, args[1], args[2], args[3])
            elif len(ctx.message.attachments) == 1:
                await editfaq(ctx, args[1], args[2], args[3], ctx.message.attachments[0].url)
    elif args[0] == "delete":
        if len(args) != 2:
            await ctx.reply('Please use it like this: `!faq delete faqname')
        else:
            await deletefaq(ctx, args[1])
    else:
        await readfaq(ctx, args[0])




bot.run(os.getenv('TOKEN'))

