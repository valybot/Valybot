import sqlite3
import os
from dotenv import load_dotenv

load_dotenv()
connection = sqlite3.connect(os.getenv('DB'))
cursor = connection.cursor()
sql = []
sql.append('''CREATE TABLE IF NOT EXISTS "commands" (
	"id"	INTEGER,
	"name"	TEXT NOT NULL,
	"contentid"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
)''')

sql.append('''CREATE TABLE IF NOT EXISTS "content" (
	"id"	INTEGER,
	"title"	TEXT NOT NULL,
	"text"	TEXT NOT NULL,
	"imgurl"	TEXT,
	PRIMARY KEY("id")
)''')

sql.append('''CREATE TABLE IF NOT EXISTS "faqs" (
	"command"	TEXT,
	"title"	TEXT NOT NULL,
	"content"	TEXT NOT NULL,
	"imgurl"	TEXT,
	PRIMARY KEY("command")
)''')

sql.append('''CREATE TABLE IF NOT EXISTS "history" (
	"oldid"	INTEGER,
	"newid"	INTEGER NOT NULL,
	PRIMARY KEY("oldid")
)''')
for i in sql:
    cursor.execute(i)

connection.commit()
connection.close()